$.select2config = {
    theme: "bootstrap",
    language: "es"
};

$.fn.selectPreloaded = function (params) {
    var result = $(this).select2($.extend({
        allowClear: params && params.allowClear != null ? params.allowClear : false,
        placeholder: params && params.placeholder != null ? params.placeholder : 'Seleccionar un valor...',
    }, $.select2config)).on('select2:close', function (e) {
        $(this).focus();
    });

    if (params && params.onSelect) {
        result.on('select2:select', function (e) {
            params.onSelect(e.params.data, arguments);
        })
    }

    if (params && params.disabled) {
        result.select2('enable', !params.disabled)
    }

    return result;
};

$.fn.selectToLoad = function (params) {
    var result = $(this).select2($.extend({
        allowClear: params && params.allowClear != null ? params.allowClear : false,
        placeholder: params && params.placeholder != null ? params.placeholder : 'Seleccionar un valor...',
        data: params.data
    }, $.select2config)).on('select2:close', function (e) {
        $(this).focus();
    });

    result.val(params.selected ? params.selected : null).trigger('change');

    if (params && params.onSelect) {
        result.on('select2:select', function (e) {
            params.onSelect(e.params.data, arguments);
        })
    }

    if (params && params.disabled) {
        result.select2('enable', !params.disabled)
    }

    return result;
};

// $.fn.selectRemote = function (params) {
//     var result = $(this).select2($.extend({
//         minimumInputLength: 3,
//         maximumInputLength: 50,
//         allowClear: params && params.allowClear != null ? params.allowClear : true,
//         placeholder: params && params.placeholder != null ? params.placeholder : 'Seleccionar un valor...',
//         ajax: {
//             dataType: 'json',
//             delay: 250,
//             url: function () {
//                 return 'resource';
//             },
//             data: function (data) {
//                 return {
//                     'filter.criteria': data.term
//                 };
//             },
//             processResults: function (data) {
//                 return {
//                     results: $.map(data.data, function (obj) {
//                         obj.id = obj.id;
//                         obj.text = obj.nombre;
//                         return obj;
//                     })
//                 };
//             }
//         }
//     }, $.select2config)).on('select2:close', function (e) {
//         $(this).focus();
//     });
//
//     if (params && params.onSelect) {
//         result.on('select2:select', function (e) {
//             params.onSelect(e.params.data, arguments);
//         })
//     }
//
//     if (params && params.disabled) {
//         result.select2('enable', !params.disabled)
//     }
//
//     return result;
// };