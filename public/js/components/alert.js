var defaultDataMapping = function(msgList) {
	return msgList.msg;
};

/**
 * 
 * @param args
 * @returns {BootstrapAlert}
 */
$.fn.alertDanger = function (args, multiple, mapping) {
	multiple = multiple || false;
	mapping= mapping || defaultDataMapping;
	var msgList= multiple ? $.map(args.message, mapping) : args.message ? [args.message] : [];
	
	this.empty();
	
	$.each(msgList, $.proxy(function(key, value){
		return new BootstrapAlert({element: $('<div/>').appendTo(this), type: 'danger', message: value, keepVisible: args.keepVisible});
	}, this));
};

/**
 *
 * @param args
 * @returns {BootstrapAlert}
 */
$.fn.alertWarning = function (args, multiple, mapping) {
	multiple = multiple || false;
	mapping= mapping || defaultDataMapping;
	var msgList= multiple ? $.map(args.message, mapping) : args.message ? [args.message] : [];
	
	this.empty();
	
	$.each(msgList, $.proxy(function(key, value){
		return new BootstrapAlert({element: $('<div/>').appendTo(this), type: 'warning', message: value, keepVisible: args.keepVisible});
	}, this));
};

/**
 *
 * @param args
 * @returns {BootstrapAlert}
 */
$.fn.alertSuccess = function (args, multiple, mapping) {
	multiple = multiple || false;
	mapping= mapping || defaultDataMapping;
	var msgList= multiple ? $.map(args.message, mapping) : args.message ? [args.message] : [];
	
	this.empty();
	
	$.each(msgList, $.proxy(function(key, value){
		return new BootstrapAlert({element: $('<div/>').appendTo(this), type: 'success', message: value, keepVisible: args.keepVisible});
	}, this));
};
