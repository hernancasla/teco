$.openModal = function (url, data) {
    var options = {
        "backdrop": 'static',
        "keyboard": true,
        "show": true
    };
    var $modal = $('<div class="custom-modal modal fade" role="dialog" aria-hidden="true"></div>');
    $modal.on('hidden.bs.modal', function (e) {
        $(document).unbind('keydown');
        $(e.target).remove();
    });
    $modal.load(url);
    if (data) {
        $modal.data('data', data);
    }
    $modal.modal(options);

    $('.modal').on('shown.bs.modal', function() {

        var modal = $(this);
        var focusableChildren = modal.find('a[href], a[data-dismiss], area[href], input, select, textarea, button, iframe, object, embed, *[tabindex], *[contenteditable]');
        var numElements = focusableChildren.length;
        var currentIndex = 0;

        $(document.activeElement).blur();

        var focus = function() {
            var focusableElement = focusableChildren[currentIndex];
            if (focusableElement)
                focusableElement.focus();
        };

        var focusPrevious = function () {
            currentIndex--;
            if (currentIndex < 0)
                currentIndex = numElements - 1;

            focus();

            return false;
        };

        var focusNext = function () {
            currentIndex++;
            if (currentIndex >= numElements)
                currentIndex = 0;

            focus();

            return false;
        };

        $(document).on('keydown', function (e) {

            if (e.keyCode == 9 && e.shiftKey) {
                e.preventDefault();
                focusPrevious();
            }
            else if (e.keyCode == 9) {
                e.preventDefault();
                focusNext();
            }
        });

        $(this).focus();
    });

    return $modal;
};