/**
 *
 * @returns {{}}
 */
$.fn.serializeObject = function () {
    var self = this,
        json = {},
        push_counters = {},
        patterns = {
            "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
            "key": /[a-zA-Z0-9_]+|(?=\[\])/g,
            "push": /^$/,
            "fixed": /^\d+$/,
            "named": /^[a-zA-Z0-9_]+$/
        };

    this.build = function (base, key, value) {
        base[key] = value;
        return base;
    };

    this.push_counter = function (key) {
        if (push_counters[key] === undefined) {
            push_counters[key] = 0;
        }
        return push_counters[key]++;
    };

    $.each($(this).serializeArray(), function () {
        // skip invalid keys
        if (!patterns.validate.test(this.name)) {
            return;
        }
        var k,
            keys = this.name.match(patterns.key),
            merge = this.value,
            reverse_key = this.name;

        while ((k = keys.pop()) !== undefined) {
            // adjust reverse_key
            reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');
            // push
            if (k.match(patterns.push)) {
                merge = self.build([], self.push_counter(reverse_key), merge);
            }
            // fixed
            else if (k.match(patterns.fixed)) {
                merge = self.build([], k, merge);
            }
            // named
            else if (k.match(patterns.named)) {
                merge = self.build({}, k, merge);
            }
        }
        json = $.extend(true, json, merge);
    });

    return json;
};

$.fn.deserializeObject = function (data) {
    var f = $(this),
        map = {},
        find = function (selector) {
            return f.is("form") ? f.find(selector) : f.filter(selector);
        };
    //Get map of values
    $.each(data.split("&"), function () {
        var nv = this.split("="),
            n = decodeURIComponent(nv[0]),
            v = nv.length > 1 ? decodeURIComponent(nv[1]) : null;
        if (!(n in map)) {
            map[n] = [];
        }
        map[n].push(v);
    })
    //Set values for all form elements in the data
    $.each(map, function (n, v) {
        var elem = find("[name='" + n + "']");
        if (elem.is('input:checkbox')) {
            elem.prop('checked', v == 'true');
        } else if (elem.is('select.select2-remote') && !elem.find('option').length) {
            $(elem).append($('<option>', {
                value: v[0],
                text: map[n + '_text'][0]
            }));
        } else if (elem.is('select.select2-hidden-accessible') && v[0]) {
            elem.val(v).trigger('change');
        } else {
            elem.val(v);
        }
    })
    //Clear all form elements not in form data
    find("input:text,select,textarea").each(function () {
        if (!($(this).attr("name") in map)) {
            $(this).val("");
        }
    })
    find("input:checkbox:checked,input:radio:checked").each(function () {
        if (!($(this).attr("name") in map)) {
            //this.$ = false;
            $(this).prop("checked", false);
        }
    })

    return this;
};

$.fn.toText = function (params) {
    var result = '';
    $.each(params.split("&"), function (idx, it) {
        var nv = it.split("="),
            n = decodeURIComponent(nv[0]),
            v = nv.length > 1 ? decodeURIComponent(nv[1]) : null,
            l = $("label[for='" + $('[name="' + n + '"]').attr('id') + "']").text();
        if ($('[name="' + n + '"]').hasClass('exclude-filter')) {
            return;
        }
        if (v && $('[name="' + n + '"]').is('select')) {
            if ($('[name="' + n + '"]').prop('multiple')) {
                v = $.map($('[name="' + n + '"] option:selected'), function (it) {
                    return $(it).text();
                }).join();
            } else {
                v = $('[name="' + n + '"] option:selected').text();
            }
        } else if ($('[name="' + n + '"]').is('input[type="checkbox"]')) {
            v = $('[name="' + n + '"]').prop('checked') ? 'Si' : 'No';
        } else if (v && $('[name="' + n + '"]').parent().hasClass('date')) {
            v = moment(v).format('DD/MM/YYYY');
        }
        var item = l && v ? l + ': ' + v + '\n' : '';
        if (!result.includes(item)) {
            result += item;
        }
    });

    return result;
};

$.fn.fillCombo = function (data, value, label) {
    var self = $(this);
    var selected = self.val();
    self.empty().append('<option value="">Seleccionar</option>')
    if (data.length) {
        $.each(data, function (index, item) {
            if (value && label) {
                var lbl = item[label];
                if ($.isFunction(label)) {
                    lbl = label(item);
                }
                self.append('<option value="' + item[value] + '"' + (selected && selected == item[value] ? 'selected' : '') + '>' + lbl + '</option>')
            } else {
                self.append('<option value="' + item + '"' + (selected && selected == item ? 'selected' : '') + '>' + item + '</option>')
            }
        });
    }

    return self;
};

$.fn.datepicker.dates['es'] = {
    days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
    daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
    today: "Hoy",
    clear: "Borrar",
    weekStart: 1,
    format: "dd/mm/yyyy"
};

$.fn.datepicker.defaultSettings = {
    format: 'dd/mm/yyyy',
    startDate: '01/01/1980',
    endDate: '30/12/2090',
    language: 'es',
    autoclose: true
};

$.filterStatusCode = function (cb, excludedErrors, jqXHR, textStatus, errorThrown) {
    excludedErrors = excludedErrors || [];
    if ($.inArray(jqXHR.status, excludedErrors) == -1) {
        cb = $.proxy(cb, this);
        cb(jqXHR, textStatus, errorThrown);
    }
};

$.paramsToObject = function (search) {
    return search ? JSON.parse('{"' + search.replace(/&/g, '","').replace(/=/g, '":"') + '"}',
        function (key, value) {
            return key === "" ? value : decodeURIComponent(value)
        }) : {}
};

$.fn.formatInteger = function () {
    return this.number(true, 0);
};

$.fn.formatDecimal = function (decimals) {
    return this.number(true, decimals ? decimals : 2);
};

$.formatInteger = function (value) {
    return $.number(value, 0, ',', '.');
};

$.formatDecimal = function (value, decimals) {
    return $.number(value, decimals ? decimals : 2, ',', '.');
};
