$.formatDate = function (dateObject) {
    var sDateOffTimezome = new Date(dateObject);
    var sDate = new Date(sDateOffTimezome.getTime() + sDateOffTimezome.getTimezoneOffset() * 60000)
    if (isNaN(sDate)) {
        sDate = dateObject.slice(0, 10);
        sDate = sDate.replace(/-/g, '/');
        sDate = new Date(sDate);
    }
    var day = sDate.getDate();
    var month = sDate.getMonth() + 1;
    var year = sDate.getFullYear();
    if (day < 10) {
        day = '0' + day;
    }
    if (month < 10) {
        month = '0' + month;
    }

    return day + '/' + month + '/' + year;
};

$.formatDateTime = function (dateObject) {
    var sDateOffTimezome = new Date(dateObject);
    var sDate = new Date(sDateOffTimezome.getTime() + sDateOffTimezome.getTimezoneOffset() * 60000)
    if (isNaN(sDate)) {
        sDate = dateObject.slice(0, 10);
        sDate = sDate.replace(/-/g, '/');
        sDate = new Date(sDate);
    }
    var day = sDate.getDate();
    var month = sDate.getMonth() + 1;
    var year = sDate.getFullYear();
    var hour = sDate.getHours();
    var minutes = sDate.getMinutes();
    var seconds = sDate.getSeconds();
    if (day < 10) {
        day = '0' + day;
    }
    if (month < 10) {
        month = '0' + month;
    }
    if (hour < 10) {
        hour = '0' + hour;
    }
    if (minutes < 10) {
        minutes = '0' + minutes;
    }
    if (seconds < 10) {
        seconds = '0' + seconds;
    }

    return day + '/' + month + '/' + year + ' ' + hour + ':' + minutes + ':' + seconds;
};

$.parseDate = function (string) {
    var array = string.split('/');
    return new Date(array[2] + '/' + array[1] + '/' + array[0]);
};

$.parseDateTime = function (string) {
    var array = string.split(' ');
    var date = array[0].split('/');
    return new Date(date[2] + '/' + date[1] + '/' + date[0] + ' ' + array[1]);
};
