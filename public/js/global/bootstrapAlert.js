/**
 *
 * @param args
 * @returns {*|jQuery|HTMLElement}
 * @constructor
 */
var BootstrapAlert = function (args) {
    if (timeout) {
        clearTimeout(timeout);
    }

    var element = $(args.element);

    element.hide();

    var tplAlert =
        '<div class="alert alert-{{type}}" role="alert">' +
        '<button type="button" class="close" data-dismiss="alert">' +
        '<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>' +
        '</button>' +
        '{{message}}' +
        '</div>';

    tplAlert = tplAlert.replace("{{type}}", args.type).replace("{{message}}", args.message);

    element.html(tplAlert);
    element.fadeTo(500, 1);

    if (!args.keepVisible) {
        timeout = window.setTimeout(function () {
            element.fadeTo(500, 0).slideUp(500, function () {
                $(this).find('.alert').remove();
                if (timeout) {
                    clearTimeout(timeout);
                }
            });
        }, 30000);
    }

    element.find('.close').on('click', function (e) {
        $(this).parent().fadeTo(500, 0).slideUp(500, function () {
            $(this).find('.alert').remove();
            if (timeout) {
                clearTimeout(timeout);
            }
        });
    });

    // Scroll to top
    (element.parents('.modal').length ? element.parents('.modal') : $("html, body")).animate({ scrollTop: 0 }, "slow");

    return element;
};

var timeout = null;