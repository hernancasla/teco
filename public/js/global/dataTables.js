//= require dataTables/jquery.dataTables
//= require dataTables/dataTables.tableTools
//= require dataTables/dataTables.bootstrap
//= require_self

$.extend($.fn.dataTableExt.oSort, {
    "string-pre": function (a) {
        var string = a;
        if (/^<.*?>$/.test(a) && !!$(a)[0]) {
            string = $(a).text();
        }
        return string.latinise().toLowerCase();
    },

    "string-asc": function (a, b) {
        return a < b;
    },

    "string-desc": function (a, b) {
        return b < a;
    }
});

$.extend($.fn.dataTableExt.oSort, {
    "number-pre": function (a) {
        var number = a;
        if (/^<.*?>$/.test(a) && !!$(a)[0]) {
            number = $(a).text();
        }
        number = (number === "" || number === " " || number === "-") ? 0 : number.toString().replace(/[^\d\,\-]/g, "").replace(',', '.');
        return parseFloat(number);
    },

    "number-asc": function (a, b) {
        return a - b;
    },

    "number-desc": function (a, b) {
        return b - a;
    }
});

$.extend($.fn.dataTableExt.oSort, {
    "date-pre": function (a) {
        var date = a;
        if (/^<.*?>$/.test(a) && !!$(a)[0]) {
            date = $(a).text();
        }
        if (date.split('/').length != 3) {
            return new Date(date);
        } else {
            date = (date === "" || date === " " || date === "-") ? null : date;
            return date ? $.parseDate(date) : null;
        }
    },

    "date-asc": function (a, b) {
        return a < b;
    },

    "date-desc": function (a, b) {
        return b < a;
    }
});

$.extend($.fn.dataTableExt.oSort, {
    "datetime-pre": function (a) {
        var date = a;
        if (/^<.*?>$/.test(a) && !!$(a)[0]) {
            date = $(a).text();
        }
        if (date.split('/').length != 3) {
            return new Date(date);
        } else {
            date = (date === "" || date === " " || date === "-") ? null : date;
            return date ? $.parseDateTime(date) : null;
        }
    },

    "datetime-asc": function (a, b) {
        return a < b;
    },

    "datetime-desc": function (a, b) {
        return b < a;
    }
});

$.fn.dataTableExt.ofnSearch['number'] = function (sData) {
    return $("<div/>").html(sData).text();
};

$.fn.dataTableExt.ofnSearch['string'] = function (sData) {
    return $("<div/>").html(sData).text();
};

$.fn.dataTableExt.ofnSearch['html'] = function (sData) {
    return $("<div/>").html(sData).text();
};

$.fn.dataTableExt.ofnSearch['date'] = function (sData) {
    return $("<div/>").html(sData).text();
};

$.fn.dataTableExt.ofnSearch['datetime'] = function (sData) {
    return $("<div/>").html(sData).text();
};

/**
 *
 * @param params { serverSide, url, pageLength = 10, columns, data }
 * @returns {*}
 */
$.fn.dataTableSettings = function (params) {
    var defaultPageLength = 10;
    var settings = {
        autoWidth: true,
        info: params.notPaging ? false : true,
        paging: params.notPaging ? false : true,
        pagingType: 'full_numbers',
        searching: true,
        lengthChange: false,
        ordering: params.notOrdering ? false : true,
        stateSave: params.notStateSave ? false : true,
        serverSide: params.serverSide ? params.serverSide : false,
        processing: false,
        order: [
            [0, 'asc']
        ],
        dom: 'C<"clear">l' + (params.hideFilter ? '' : 'f') + 'rt<"dataTables_footer"ip>',
        pageLength: params.pageLength ? params.pageLength : defaultPageLength,
        language: {
            processing: 'Procesando...',
            emptyTable: 'No hay datos disponibles en la tabla',
            info: 'Mostrando _START_ a _END_ de _TOTAL_ registros',
            infoEmpty: 'Mostrando 0 a 0 de 0 registros',
            infoFiltered: params.columnsSearch || !params.hideFilter ? '(filtrado de un total de _MAX_ registros)' : '',
            loadingRecords: 'Cargando...',
            search: '',
            paginate: {
                first: '<<',
                last: '>>',
                next: '>',
                previous: '<'
            },
            "sZeroRecords": "No se encontraron datos"
        },
        stateLoadParams: function (settings, data) {
            // Reset de busqueda
            if (params.resetSearch) {
                data.columns.every(function (item) {
                    item.search.search = '';
                });
                data.search.search = '';
            }
            // Reset de paginacion
            if (params.resetPage) {
                data.start = 0;
            }
        }
    };
    if (params.rowsGroup) {
        settings.rowsGroup = params.rowsGroup;
    }
    if (params.order) {
        settings.order = params.order;
    }

    if (params.rowCallback) {
        settings.rowCallback = params.rowCallback;
    }

    if (params.fnFooterCallback) {
        settings.fnFooterCallback = params.fnFooterCallback
    }

    if (params.drawCallback) {
        settings.drawCallback = params.drawCallback
    }

    settings.buttons = [];
    settings.buttons.push({
        type: 'excel',
        label: 'Excel',
        extend: 'excelHtml5',
        title: function () {
            return params.getExportTitle();
        },
        filename: function () {
            return params.getExportFileName();
        },
        exportOptions: {
            format: {
            	body: function(data, row, column, node) {
            		data = $('<p>' + data + '</p>').text();
                    return $.isNumeric(data.replace(/[^\d\,\-]/g, "").replace(',', '.')) ? parseFloat(data.replace(/[^\d\,\-]/g, "").replace(',', '.')) : data;
                }
             },
             columns: ':visible'
        }
    });

    if (params.singleSelection || params.multipleSelection) {
        settings.language.select = {
            rows: {
                _: "%d registros seleccionados",
                0: "Clickee una fila para seleccionarla",
                1: "1 registro seleccionado"
            }
        }
        if (params.singleSelection) {
            settings.select = {
                style: 'single',
                blurable: true
            }
        } else if (params.multipleSelection) {
            settings.select = {
                style: 'os',
                blurable: true
            }
        }
    } else {
        settings.select = false;
    }

    if (params.columnsSearch) {
        settings.initComplete = function () {
            if ($(this.api().table().header()).find('tr.search').length) {
                $(this.api().table().header()).find('tr.search').remove();
            }
            var html = '<tr class="search visible-lg visible-md" role="row">';
            this.api().columns().every(function (item) {
                // Reset de busqueda
                if (params.resetSearch) {
                    this.search('').draw();
                }
                html += '<th style="border-top: 1px solid #dddddd;">';
                if (this.settings()[0].aoColumns[this[0][0]].bSearchable) {
                    var search = this.search();
                    if (search.length) {
                        if (search.substring(0, 1) == "^") {
                            search = search.substring(1);
                        } else {
                            search = "*" + search;
                        }
                    }
                    html += '<input id="' + this.index() + '" type="text" class="form-control" style="font-weight: initial; width:100%;" value="' + search + '" placeholder="Buscar"/>';
                } else {
                    html += '&nbsp;'
                }
                html += '</th>'
            });
            html += '</tr>'
            $(this.api().table().header()).find('tr').before(html);
            this.api().columns().every(function () {
                var that = this;
                $(this.header()).parents('thead').find('tr.search #' + that.index()).on('keyup change', function () {
                    var criteria = this.value;
                    if (criteria.trim() == '*') {
                        return false;
                    }
                    var regex = false;
                    if (criteria.trim() != '' && !/^\*/g.test(criteria)) {
                        criteria = "^" + criteria + "";
                        regex = true;
                    } else {
                        criteria = criteria.substring(1)
                    }
                    that.search(criteria, regex, regex).draw();
                });
            });
        }
    }

    if (params.serverSide) {
        settings = $.extend({
            ajax: {
                url: params.url,
                contentType: 'application/json',
                type: 'get',
                data: function (d) {
                    var data = {
                        count: d.length,
                        offset: d.start,
                        sort: d.columns[d.order[0].column].name,
                        order: d.order[0].dir,
                        draw: d.draw
                    };
                    if (params.filters) {
                        $.each(params.filters, function (index, value) {
                            data['filter.' + index] = value;
                        });
                    } else {
                        data.filter = d.search.value
                    }
                    return data;
                },
                dataSrc: function (json) {
                    json.recordsTotal = json.total;
                    json.recordsFiltered = json.total;
                    return json.data;
                }
            }
        }, settings);
    }

    if (params.data) {
        settings = $.extend({data: params.data}, settings);
    }

    if (params.scroll) {
        settings.scrollY = '49vh';
        settings.scrollX = true;
        settings.scrollCollapse = true;
        settings.scroller = true;
        settings.deferRender = true;
    }

    var columns = params.columns;

    if (params.actions && params.actions.length) {
        var columnActions = $.grep(params.actions, function (value, index) {
            return value.column;
        });
        if (columnActions.length) {
            columns.push({
                orderable: false,
                searchable: false,
                type: 'html',
                title: '',
                defaultContent: '-',
                className: 'left table-actions',
                width: (columnActions.length * 25),
                render: function (data, type, full, meta) {
                    var links = [];
                    links.push('')
                    $.each(columnActions, function (index, action) {
                        links.push('<a class="' + action.id + ' menu-icon" href="#"><span title="' + action.title + '" style="font-size: 17px;" class="glyphicon ' + action.icon + '"></span></a>');
                    });
                    return links.join('&nbsp;&nbsp;');
                },
                createdCell: function (td, cellData, rowData, row, col) {
                    $(td).attr('data-title', 'Acciones')
                }
            });
        }
    }

    return $.extend({columns: columns}, settings);
};

/**
 *
 * @param params { serverSide, url, pageLength = 10, columns, data }
 * @returns {{}}
 */
$.fn.renderDataTable = function (params) {
    if (params.fnFooterCallback) {
        $(this).find('tfoot').remove();
        var footer = document.createElement('tfoot');
        var tr = document.createElement('tr');
        $.each(params.columns, function (i, value) {
            var th = document.createElement('th');
            tr.appendChild(th);
        });
        if (params.actions && params.actions.length) {
            var columnActions = $.grep(params.actions, function (value, index) {
                return value.column;
            });
            $.each(columnActions, function (i, value) {
                var th = document.createElement('th');
                tr.appendChild(th);
            });
        }
        footer.appendChild(tr);
        $(footer).addClass('hidden-xs');
        $(this).append(footer);
    }

    var result = $(this).DataTable($.fn.dataTableSettings(params));

    var container = $(this).parents('.dataTables_wrapper');
    container.find('.dataTables_filter input').addClass('input-sm').attr('placeholder', 'Buscar');

    // Botones de acciones
    if (params.actions && params.actions.length) {
        var singleActions = [];
        var menuActions = [];

        if (params.actions && params.actions.length) {
            // Acciones que se agregan en forma independiente
            singleActions = $.grep(params.actions, function (value, index) {
                return !value.menu && !value.column;
            });

            // Acciones que se agregan en el dropdown
            menuActions = $.grep(params.actions, function (value, index) {
                return value.menu;
            });
        }

        result.on('select', function (e, dt, type, indexes) {
            // var rowData = result.rows(indexes).data().toArray();
            $(result.body()[0]).parents('.dataTables_wrapper').find('#dropdownMenuHeaderActions').prop('disabled', result.rows({selected: true}).count() != 1);
            if (params.onSelectRow && arguments[0].length) {
                params.onSelectRow(arguments[0]);
            }
        });

        result.on('deselect', function (e, dt, type, indexes) {
            //var rowData = result.rows(indexes).data().toArray();
            $(result.body()[0]).parents('.dataTables_wrapper').find('#dropdownMenuHeaderActions').prop('disabled', result.rows({selected: true}).count() != 1);
            if (params.onDeselectRow && arguments[0].length) {
                params.onDeselectRow(arguments[0]);
            }
        });


        // Render del contenedor de botones
        var html = '';
        html += '<div class="container-fluid">';
        html += '   <div class="row">';

        // Render de botones que se agregan en forma independiente
        var count = singleActions.length + (menuActions.length ? 1 : 0);
        var colSize = '6';
        if (count == 1) {
            colSize = '12';
        }

        if (singleActions.length) {
            $.each(singleActions, function (index, action) {
                html += '       <div class="col-lg-2 col-md-2 col-sm-' + colSize + ' col-xs-' + colSize + ' nopadding">';
                if (!action.options) {
                    html += '           <button id="btn-' + action.id + '" class="btn btn-primary btn-block btn-sm" tabindex="' + (100 + index + 1) + '">';
                    html += '               <span class="glyphicon ' + action.icon + '"></span>';
                    html += '               ' + action.title;
                    html += '           </button>';
                } else {
                    html += '       <div class="dropdown">';
                    html += '           <button class="btn btn-primary btn-block btn-sm dropdown-toggle" type="button" id="dropdownMenuHeader' + action.id + '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" tabindex="' + (100 + index + 1) + '">';
                    html += '               ' + action.title;
                    html += '               <span class="caret"></span>';
                    html += '           </button>';
                    html += '           <ul class="dropdown-menu" aria-labelledby="dropdownMenuHeader' + action.id + '" style="margin: 0px 0px 0px -1px;">';
                    $.each(action.options, function (idx, option) {
                        html += '                   <li><a href="#" id="btn-' + option.id + '">';
                        html += '                       ' + option.label;
                        html += '                   </a></li>';
                    });
                    html += '           </ul>';
                    html += '       </div>';
                }
                html += '       </div>';
            });
        }

        // Render de botones que se agregan en el dropdown
        if (menuActions.length) {
            var tabindex = (100 + singleActions.length + 1);
            html += '       <div class="col-lg-2 col-md-2 col-sm-' + colSize + ' col-xs-' + colSize + ' nopadding">';
            html += '       <div class="dropdown">';
            html += '           <button class="btn btn-primary btn-block btn-sm dropdown-toggle" type="button" id="dropdownMenuHeaderActions" disabled="disabled" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" tabindex="' + tabindex + '">';
            html += '               Acciones';
            html += '               <span class="caret"></span>';
            html += '           </button>';
            html += '           <ul class="dropdown-menu" aria-labelledby="dropdownMenuHeaderActions" style="margin: 0px 0px 0px -1px;">';
            $.each(menuActions, function (index, action) {
                if (action.divider) {
                    html += '                   <li role="separator" class="divider"></li>';
                }
                html += '                   <li><a href="#" id="btn-' + action.id + '" tabindex="' + (tabindex + index + 1) + '">';
                html += '                       <span class="glyphicon ' + action.icon + '"></span>';
                html += '                       ' + action.title;
                html += '                   </a></li>';
            });
            html += '           </ul>';
            html += '       </div>';
            html += '       </div>';

        }
        html += '   </div>';
        html += '</div>';

        $(result.body()[0]).parents('.dataTables_wrapper').prepend(html);

        var container = $(result.body()[0]).parents('#responsive-table').parent().find('.container-fluid');

        if (container.find('button').length % 2 && container.find('button').length > 2) {
            $(container.find('button')[container.find('button').length - 1]).closest('.nopadding').removeClass('col-sm-6').removeClass('col-xs-6').addClass('col-sm-12').addClass('col-xs-12');
        }

        // Bind de evento click a todos los botones
        $.each(params.actions, function (index, action) {
            if (!action.column || (action.column && action.menu) || (!action.column && !action.menu)) {
                if (!action.options) {
                    container.find('#btn-' + action.id).on('click', function (e) {
                        if (result.rows({selected: true}).count()) {
                            action.onClick(e, result.rows({selected: true}).data()[0])
                        } else {
                            action.onClick(e)
                        }
                    });
                } else {
                    $.each(action.options, function (idx, option) {
                        container.find('#btn-' + option.id).on('click', function (e) {
                            option.onClick(e)
                        });
                    });
                }
            }
            if (action.column) {
                $(result.body()[0]).off('click', 'tr > td a.' + action.id).on('click', 'tr > td a.' + action.id, function (e) {
                    e.stopPropagation();
                    $(result.body()[0]).parents('.dataTables_wrapper').find('#dropdownMenuHeaderActions').prop('disabled', menuActions.length);
                    action.onClick(e, result.row($(this).parents('tr')).data());
                });
            }
        });
    }

    // Info de seleccion multiple y bindeo de boton todos
    if (params.multipleSelection) {
        $('<span class="dataTables_selection"><a href="#" id="select-total-rows">(Todos)</a></span>').insertBefore($(result.body()[0]).parents().find('span.select-info'));
        var todos = false;
        $(result.body()[0]).parents().find('span.dataTables_selection').find('#select-total-rows').on('click', function (e) {
            e.preventDefault();
            if (!todos) {
                $(this).text('(Ninguno)');
                result.rows().select();
                todos = true;
                if (params.onSelectAll) {
                    params.onSelectAll();
                }
            } else {
                $(this).text('(Todos)');
                result.rows().deselect();
                todos = false;
                if (params.onDeselectAll) {
                    params.onDeselectAll();
                }
            }
            $(e.target).parents().find('div.dataTables_wrapper').find('#dropdownMenuHeaderActions').prop('disabled', result.rows({selected: true}).count() != 1);
        });
    }

    if (params.singleSelection || params.multipleSelection) {
        $(result.body()[0]).find('tr > td').css('cursor', 'pointer');
    }

    result.exportExcel = function (filename, title) {
        if (!filename) {
            throw 'Missing filename';
        }
        params.getExportFileName = function () {
            return filename;
        }
        params.getExportTitle = function () {
            return title ? title : null;
        }
        result.button(0).trigger();
    };

    return result;
};