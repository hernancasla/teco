/**
 *
 * @param value
 * @param validator
 * @param $field
 * @returns {{valid: boolean, message: string}}
 */
var callbackDecimal = function (value, validator, $field) {
    return {
        valid: Number(value).getIntegers() < 16 && Number(value).getDecimals() < 3,
        message: 'El valor puede contener hasta 15 enteros y 2 decimales'
    }
};

var callbackDecimalMaxLength15 = function (value, validator, $field) {
    return {
        valid: Number(value).getIntegers() <= 15 && Number(value).getDecimals() < 3,
        message: 'El valor puede contener hasta 15 enteros y 2 decimales'
    }
};

$.app = {};

$.app.validators = {};

$.app.validators.notEmpty = {
    message: 'El campo es requerido'
};

$.app.validators.notEmptyValue = {
    notEmpty: $.app.validators.notEmpty
};

$.app.validators.stringMaxLength = {
    max: 50,
    message: 'El campo debe tener un m&aacute;ximo de 30 caracteres de longitud'
};

$.app.validators.stringValue = {
    stringLength: $.app.validators.stringMaxLength
}

$.app.validators.stringRequired = {
    notEmpty: $.app.validators.notEmpty,
    stringLength: $.app.validators.stringMaxLength
};

$.app.validators.numericPositive = {
    value: 0.0001,
    message: 'El valor debe ser mayor a cero (0)'
}

$.app.validators.numericRequired = {
    notEmpty: $.app.validators.notEmpty,
    numeric: $.app.validators.numeric
};

$.app.validators.numeric = {
    message: 'El campo debe ser un valor num&eacute;rico'
};

$.app.validators.decimal = {
    numeric: $.app.validators.numeric,
    callback: {
        callback: callbackDecimal
    }
};

$.app.validators.decimalRequired = {
    notEmpty: $.app.validators.notEmpty,
    numeric: $.app.validators.numeric,
    callback: {
        callback: callbackDecimal
    }
};

$.app.validators.decimalPositive = {
    numeric: $.app.validators.numeric,
    greaterThan: $.app.validators.numericPositive,
    callback: {
        callback: callbackDecimal
    }
};

$.app.validators.decimalPositiveRequired = {
    notEmpty: $.app.validators.notEmpty,
    numeric: $.app.validators.numeric,
    greaterThan: $.app.validators.numericPositive,
    callback: {
        callback: callbackDecimal
    }
};

$.app.validators.percentage = {
    numeric: $.app.validators.numeric,
    callback: {
        callback: callbackDecimal
    },
    between: {
        min: 0.0,
        max: 100.0,
        message: 'El valor debe ser entre 0 y 100'
    }
};

$.app.validators.percentageRequired = {
    notEmpty: $.app.validators.notEmpty,
    numeric: $.app.validators.numeric,
    callback: {
        callback: callbackDecimal
    },
    between: {
        min: 0.0,
        max: 100.0,
        message: 'El valor debe ser entre 0 y 100'
    }
};

$.app.validators.integer = {
    message: 'El campo debe ser un valor entero'
};

$.app.validators.digits = {
    digits: $.app.validators.integer
};

$.app.validators.digitsRequired = {
    notEmpty: $.app.validators.notEmpty,
    digits: $.app.validators.integer
};

$.app.validators.digitsPositive = {
    digits: $.app.validators.integer,
    greaterThan: $.app.validators.numericPositive
};

$.app.validators.digitsPositiveRequired = {
    notEmpty: $.app.validators.notEmpty,
    digits: $.app.validators.integer,
    greaterThan: $.app.validators.numericPositive
};

$.app.validators.emailAddressRegexp = {
    regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
    message: 'El valor no es una direcci&oacute;n de email v&aacute;lida'
};

$.app.validators.emailAddress = {
    regexp: $.app.validators.emailAddressRegexp
};

$.app.validators.emailAddressRequired = {
    notEmpty: $.app.validators.notEmpty,
    regexp: $.app.validators.emailAddressRegexp
};

$.app.validators.cuitRegexp = {
    regexp: '^[0-9]{2}-[0-9]{8}-[0-9]{1,2}$',
    message: 'El valor no es un CUIT v&aacute;lido'
};

$.app.validators.cuitRequired = {
    notEmpty: $.app.validators.notEmpty,
    regexp: $.app.validators.cuitRegexp
};

$.app.validators.phoneRegexp = {
    regexp: /^(((\+54|\+549)(0?)[1-9]{2,3})|(0?[1-9]{2,3}))?[0-9]{6,10}$/i,
    message: 'El valor no es un n&uacute;mero telef&oacute;nico v&aacute;lido'
};

$.app.validators.phone = {
    regexp: $.app.validators.phoneRegexp
};

$.app.validators.phoneRequired = {
    notEmpty: $.app.validators.notEmpty,
    regexp: $.app.validators.phoneRegexp
};

$.app.validators.date = {
    format: 'DD/MM/YYYY',
    //min: '01/01/2010',
    //max: '12/30/2020',
    message: 'El valor no es una fecha v&aacute;lida'
}

$.app.validators.month = {
    format: 'MMMM',
    //min: '01/01/2010',
    //max: '12/30/2020',
    message: 'El valor no es un mes v&aacute;lido'
}

$.app.validators.year = {
    format: 'YYYY',
    //min: '01/01/2010',
    //max: '12/30/2020',
    message: 'El valor no es una a\u00F1o v&aacute;lido'
}

$.app.validators.dateValue = {
    date: $.app.validators.date
}

$.app.validators.dateRequired = {
    notEmpty: $.app.validators.notEmpty,
    date: $.app.validators.date
}
