$.widget('dnt.login', {

    _create: function () {
        this._initialize();
    },

    _initialize: function () {
        this.element.find('#login-form').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                username: {
                    validators: $.app.validators.stringRequired
                },
                password: {
                    validators: $.app.validators.stringRequired
                }
            }
        });
    },

    destroy: function () {
    }

});
