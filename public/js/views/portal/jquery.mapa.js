$.widget( 'teco.mapa', {

    options: {
        key: 'AIzaSyCjWnMj6_BuVTw75vXu4GF714zhIqvJEpk',
        url: '/backoffice/api/oficinas?',
        filter:'',
        maxDistancia: 5, // en kms
        mapaDetail: $([]),
        styles : [ {
            featureType : "poi",
            stylers : [ {
                visibility : "off"
            } ]
        } ]
    },

    DIALOG: {
        MAPA: {
//			title: 'Mapa',
            modal: false,
            stack: true,
            closeOnEscape: true,
            close: function(event, ui){
                $(this).remove();
                $('body').unblock();
            }
        }
    },

    _create: function () {
        this.bindVars();
        this.init();
    },

    bindVars: function() {
        this.prestadoresMarkers = [];
        this.divMapa = $('<div>Cargando...</div>');
    },

    init: function() {
        this.element.html(this.divMapa);

        this.divMapa.css( { height:this.element.height(), width: this.element.width() } );

        setTimeout( $.proxy(this._onLoadMapDelay, this), 200 );
    },

    _onLoadMapDelay: function() {
        if( !this.map ) {
            this.map = new google.maps.Map( this.divMapa[0], { mapTypeControl: false,
                panControl: false, streetViewControl: false } );
            this.map.setOptions({styles: this.options.styles});
        }
        this.map.setCenter(new google.maps.LatLng(-38.556475083689975, -60.735021474884036));
        this.map.setZoom(3);
        this.map.setMapTypeId(google.maps.MapTypeId.ROADMAP);
        $.getJSON( this.options.url+jQuery.param(JSON.parse(this.options.filter)), {}, $.proxy( this._onLoadPrestadores, this ) );
        this.bindEvents();
    },

    _onLoadPrestadores: function( data ) {
        this.prestadores = data;
        if( this.marker ) {
            this._addPrestadoresMarkers(this.marker.position.latitude, this.marker.position.longitude);
        }
    },

    _addPrestadoresMarkers:function(){
        var lat = this.marker.position.lat();
        var long = this.marker.position.lng();

        this._cleanPrestadores();
        var infowindow = new google.maps.InfoWindow();
        var bounds = new google.maps.LatLngBounds();

        var extraBoundsCount=0;
        for( pos in this.prestadores ) {
            prestadores = this.prestadores;
            var coord = this.prestadores[pos].coordenadas.split(',');


            this.prestadores[pos].distancia = this._distanciaKms( parseFloat(coord[0]), parseFloat(coord[1]), lat, long );
//			if( this.prestadores[pos].distancia > this.options.maxDistancia ) {
//				continue;
//			}


            var posicionCoordenadas = new google.maps.LatLng(coord[0], coord[1]);
            marker = new google.maps.Marker({
                position: posicionCoordenadas,
                map: this.map,
                title: this.prestadores[pos].nombre + " - " + this.prestadores[pos].direccion,
                data: this.prestadores[pos],
                flat: true,
                draggable: false
            });
            this.prestadoresMarkers.push( marker );

            google.maps.event.addListener(marker, 'click', (function (marker, pos) {
                return function () {
                    var strContent="<div class='geoMarcaPopup' style='font-family: Arial; font-size: 12px'>";
                    strContent += "<div style='font-weight: bold'>"+prestadores[pos].nombre+"</div>";
                    strContent += prestadores[pos].tipo+"<br>";
                    strContent += prestadores[pos].direccion + " - " + prestadores[pos].localidad +"<br>";
                    strContent += prestadores[pos].horario +"<br>";
                    strContent += 'Distancia: ' + prestadores[pos].distancia.toFixed(1) + " Kms";

                    infowindow.setContent(strContent);
                    infowindow.open(this.map, marker);
                }
            })(marker, pos));

            var iconFile = "http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png";
            marker.setIcon(iconFile);
            bounds.extend(marker.position);
            extraBoundsCount++;
        }

        this._loadMapaDetail();
        //Prevent issue with zoom with a single element.
        if(extraBoundsCount>0){
//			this.map.fitBounds(bounds);
        }
    },

    showPosition: function(position, descr) {
        this._cleanCurrentPosition();
        var posicionCoordenadas = new google.maps.LatLng(position.latitude, position.longitude);

        this.map.setCenter(posicionCoordenadas);
        this.map.setZoom(13);

        this.marker = new google.maps.Marker({
            position: posicionCoordenadas,
            map: this.map,
            flat: true,
            title: descr,
            draggable: false
        });

        var iconFile = "http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png";
        this.marker.setIcon(iconFile);
        var bounds = new google.maps.LatLngBounds();
        bounds.extend(this.marker.position);

        this._addPrestadoresMarkers();
        this._onMapPositionChanged(); //NEW
//		this.map.fitBounds(bounds);
    },

    _loadMapaDetail: function() {
        this.options.mapaDetail.html('');

        this.prestadoresMarkers = this.prestadoresMarkers.sort(function(a, b){
            return ((a.data.distancia < b.data.distancia) ? -1 : (a.data.distancia > b.data.distancia) ? 1 : 0);
        });

        for( var pos in this.prestadoresMarkers ) {
            var div = $('<div></div>').attr('id', 'prestador_' + pos ).hide();

            div.css( {'border': '2px solid #aaa', 'margin-bottom': '3px', 'border-radius': '6px', padding: '2px 6px',
                'background-color': '#def', 'cursor': 'pointer' } );

            var title = $('<div></div>').text( this.prestadoresMarkers[pos].data.nombre );
            title.css( { 'font-family': "Sans", 'font-size': '13px', 'font-weight': 'bold',
                'border-bottom': '2px solid #aaa', 'margin-bottom': '5px' } );

            var body = $('<div></div>').html( this.prestadoresMarkers[pos].data.tipo+"</br>"
                + this.prestadoresMarkers[pos].data.direccion + " - " + this.prestadoresMarkers[pos].data.localidad +"</br>"
                + this.prestadoresMarkers[pos].data.horario +"</br>"
                + 'Distancia: ' + this.prestadoresMarkers[pos].data.distancia.toFixed(1) + " Kms" );
            body.css( { 'font-family': "Sans", 'font-size': '11px' } );

            div.append( title ).append( body );
            this.options.mapaDetail.append( div );
            this._onMapPositionChanged(); //NEW
        }

        this.options.mapaDetail.find('>div').click( $.proxy(this._onMapDetailClick, this) );
    },

    _onMapPositionChanged: function() {
        if( this.timeout ) {
            clearTimeout( this.timeout );
        }

        this.timeout = setTimeout( $.proxy(this._onMapPositionChangedDelay, this), 50 );
    },

    _onMapDetailClick: function(e) {
        var id = $(e.currentTarget).attr('id');
        var pos = parseInt(id.split('_')[1]);

//		var latLng = this.prestadoresMarkers[pos].getPosition(); // returns LatLng object
//		this.map.setCenter(latLng); // setCenter takes a LatLng object
        new google.maps.event.trigger( this.prestadoresMarkers[pos], 'click' );
    },

    _onMapPositionChangedDelay: function() {
        var count = 0;
        for( var pos in this.prestadoresMarkers ) {
            if( this.map.getBounds().contains(this.prestadoresMarkers[pos].getPosition()) ) {
                this.options.mapaDetail.find('#prestador_'+pos).slideDown();
            } else {
                this.options.mapaDetail.find('#prestador_'+pos).slideUp();
            }
        }
    },

    _cleanCurrentPosition: function() {
        if( this.marker ) {
            this.marker.setMap(null);
        }
    },

    _cleanPrestadores: function() {
        for( var pos in this.prestadoresMarkers ) {
            this.prestadoresMarkers[pos].setMap(null);
        }
        this.prestadoresMarkers = [];
    },

    _distanciaKms: function(lat1,lon1,lat2,lon2) {
        rad = function(x) {return x*Math.PI/180;}
        var R = 6378.137; //Radio de la tierra en km
        var dLat = rad( lat2 - lat1 );
        var dLong = rad( lon2 - lon1 );
        var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c;
        return d;
//		return d.toFixed(3); //Retorna tres decimales
    },

    bindEvents: function() {
        this.map.addListener('bounds_changed', $.proxy(this._onMapPositionChanged, this) );
        this.map.addListener('center_changed', $.proxy(this._onMapPositionChanged, this) );
    },

    _onClickBtnPrestacion:function(e){
    },

    _setOption: function ( key, value ) {
        if( key == 'url' ) {
            this.options.url = value;
            $.getJSON( this.options.url, {}, $.proxy( this._onLoadPrestadores, this ) );
        }
    },

    destroy: function() {
        $('body').unblock();
        $('html, body').css({ overflow: 'auto', height: 'auto' });
    }

} );