$.widget('teco.oficinaForm', {

    options: {},

    _create: function () {
        this._bindVars();
        this._bindEvents();
        this._initialize();
    },

    _bindVars: function () {
        this.form = this.element.find('#frm-oficina');
    },

    _bindEvents: function () {

        this.form.find('#comprar-lineas').on('change', $.proxy(this._changeComboBox, this,this.form.find('#comprar-lineas')));
        this.form.find('#comprar-celular').on('change', $.proxy(this._changeComboBox, this,this.form.find('#comprar-celular')));
        this.form.find('#pasar-personal').on('change', $.proxy(this._changeComboBox, this,this.form.find('#pasar-personal')));
        this.form.find('#recuperar-chip').on('change', $.proxy(this._changeComboBox, this,this.form.find('#recuperar-chip')));
        this.form.find('#mejorar-plan').on('change', $.proxy(this._changeComboBox, this,this.form.find('#mejorar-plan')));
        this.form.find('#tipo').selectPreloaded({allowClear: true});
        this.form.find('#provincia').selectPreloaded({allowClear: true});
        this.form.find('#tipo').selectPreloaded({allowClear: true});
        this.form.find('#localidad').selectPreloaded({allowClear: true});
    },

    _initialize: function () {
        this.form.find('#tipo').trigger('change');
        this.form.find('#provincia').trigger('change');

        this.form.formValidation({
            framework: 'bootstrap',
            fields: {
                nombre: {
                    validators: $.app.validators.stringRequired
                },
                direccion: {
                    validators: $.app.validators.stringRequired
                },
                localidad: {
                    validators: $.app.validators.stringRequired
                },
                coordenadas: {
                    validators: $.app.validators.stringRequired
                }
            },
            onSuccess: $.proxy(this._saveOrUpdate, this)
        });

        this.form.find('#nombre').focus();
    },
    _changeComboBox: function (element,event) {
        element.prop('value',$(event.currentTarget).is(':checked')?'1':'0');
    },
    _saveOrUpdate: function (e) {
        e.preventDefault();
        var id = this.element.find('#id').val();
        var formData = this.form.serializeObject();
        var url = 'api/oficinas';
        var options = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'POST',
            url: url
        };
        if (id) {
            options = {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'PUT',
                url: url + '/' + formData.id
            };
        }

        $.ajax($.extend(options, {
            data: JSON.stringify(formData),
            dataType: 'json',
            statusCode: {
                400: $.proxy(this._saveOrUpdateValidationCallback, this)
            },
            success: $.proxy(this._saveOrUpdateSuccessCallback, this),
            error: $.proxy($.filterStatusCode, this, this._saveOrUpdateErrorCallback, [400])
        }));
    },

    _saveOrUpdateValidationCallback: function (jqXHR) {
        this.element.find('#messages-block').alertWarning({message: jqXHR.responseJSON, keepVisible: true}, true);
        this.element.find('button[type=submit].btn').prop('disabled', false).removeClass('disabled');
    },

    _saveOrUpdateSuccessCallback: function (data, textStatus, jqXHR) {
        this.element.parents('.modal').trigger('saved', data).modal('toggle');
    },

    _saveOrUpdateErrorCallback: function (jqXHR, textStatus, errorThrown) {
        this.element.find('#messages-block').alertDanger({message: jqXHR.responseJSON, keepVisible: true}, true);
        this.element.find('button[type=submit].btn').prop('disabled', false).removeClass('disabled');
    },

    destroy: function () {
    }

});



