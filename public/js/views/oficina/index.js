$.widget('teco.oficinaIndex', {

    options: {},

    _create: function () {
        this._bindVars();
        this._initialize();
        this._bindEvents();
        this._getData();
    },

    _bindVars: function () {
        this.dataTable = null;
        this.formFilter = this.element.find('#form-filter');
        this.clearBtn = this.element.find('#clear-btn');
    },

    _bindEvents: function () {
        this.clearBtn.click($.proxy(this._clear, this));
    },

    _initialize: function () {
        this.formFilter.find('#tipo-filter').selectPreloaded({allowClear: true});
        this.formFilter.find('#provincia-filter').selectPreloaded({allowClear: true});


        this.formFilter.formValidation({
            framework: 'bootstrap',
            onSuccess: $.proxy(this._search, this)
        });
    },
    _getData: function () {
        $.ajax({
            method: 'GET',
            url: 'api/oficinas?' + $.teco.urlFilter,
            dataType: 'json',
            statusCode: {
                400: $.proxy(this._getDataValidationCallback, this)
            },
            success: $.proxy(this._getDataSuccessCallback, this),
            error: $.proxy(this._getDataErrorCallback, this)
        });
    },

    _getDataSuccessCallback: function (data, textStatus, jqXHR) {
        var list = data ? data : [];

        var columns = [
            {
                data: 'tipo',
                orderable: true,
                searchable: true,
                type: 'string',
                title: 'Tipo',
                defaultContent: '-',
                className: 'left',
                render: function (data, type, full, meta) {
                    return data!=null?data:'-';
                },
                createdCell: function (td, cellData, rowData, row, col) {
                    $(td).attr('data-title', 'Tipo');
                }
            },
            {
                data: 'nombre',
                orderable: true,
                searchable: true,
                type: 'string',
                title: 'Nombre',
                defaultContent: '-',
                className: 'left',
                render: function (data, type, full, meta) {
                    return data!=null?data:'-';
                },
                createdCell: function (td, cellData, rowData, row, col) {
                    $(td).attr('data-title', 'Nombre');
                }
            },
            {
                data: 'provincia',
                orderable: true,
                searchable: true,
                type: 'string',
                title: 'Provincia',
                defaultContent: '-',
                className: 'left',
                render: function (data, type, full, meta) {
                    return data!=null?data:'-';
                },
                createdCell: function (td, cellData, rowData, row, col) {
                    $(td).attr('data-title', 'Provincia');
                }
            },
            {
                data: 'horario',
                orderable: true,
                searchable: true,
                type: 'string',
                title: 'Horario',
                defaultContent: '-',
                className: 'left',
                render: function (data, type, full, meta) {
                    return data!=null?data:'-';
                },
                createdCell: function (td, cellData, rowData, row, col) {
                    $(td).attr('data-title', 'Horario');
                }
            }

        ];
        var actions = [
            {
                id: 'create',
                title: 'Nuevo',
                icon: 'glyphicon-plus',
                onClick: $.proxy(this._new, this)
            },
            {
                id: 'read',
                title: 'Consultar',
                icon: 'glyphicon-eye-open',
                column: true,
                menu: true,
                onClick: $.proxy(this._read, this)
            },
            {
                id: 'edit',
                title: 'Modificar',
                icon: 'glyphicon-edit',
                column: true,
                menu: true,
                onClick: $.proxy(this._edit, this)
            },
            {
                id: 'eliminar',
                title: 'Eliminar',
                icon: 'glyphicon-remove',
                menu: true,
                onClick: $.proxy(this._delete, this)
            }
        ];

        if (this.dataTable) {
            this.dataTable.destroy();
            this.dataTable = null;
        }

        this.dataTable = this.element.find('.table').renderDataTable({
            serverSide: false,
            order: [[0, 'desc'], [1, 'desc'], [2, 'desc'], [3, 'desc']],
            pageLength: 24,
            columns: columns,
            hideFilter: true,
            data: list,
            columnsSearch: false,
            singleSelection: true,
            actions: actions
        });

        this.formFilter.find('#buscar-btn').prop('disabled', false).removeClass('disabled');
    },


    _getDataValidationCallback: function (jqXHR) {
        $('#messages-block').alertWarning({message: jqXHR.responseJSON}, true);
        this.element.find('button[type=submit].btn').prop('disabled', false).removeClass('disabled');
    },

    _getDataErrorCallback: function (jqXHR, textStatus, errorThrown) {
        $('#messages-block').alertDanger({message: jqXHR.responseText, keepVisible: true});
        this.element.find('button[type=submit].btn').prop('disabled', false).removeClass('disabled');
    },

    _revalidateField: function (name) {
        this.formFilter.formValidation('revalidateField', name);
    },

    _new: function (e) {
        e.preventDefault();
        var $modal = $.openModal('oficina/create');
        $modal.on('saved', $.proxy(this._newCallback, this));
    },

    _newCallback: function (e, obj) {
        $('#messages-block').alertSuccess({message: 'Se ha dado de alta una nueva oficina ' + obj.nombre});
        this._getData();
    },

    _edit: function (e, data) {
        e.preventDefault();
        if (!data) {
            $('#messages-block').alertWarning({message: 'Debe seleccionar un &iacute;tem'});
            return;
        }
        if(!data.id){
            $('#messages-block').alertWarning({message: 'El &iacute;tem seleccionado no esta creado'});
            return;
        }
        var $modal = $.openModal('oficina/' + data.id + '/edit');
        $modal.on('saved', $.proxy(this._editCallback, this));
    },

    _editCallback: function (e, obj) {
        $('#messages-block').alertSuccess({message: 'Se ha actualizado la oficina'});
        this._getData();
    },

    _delete: function (e, data) {
        e.preventDefault();
        if (!data) {
            $('#messages-block').alertWarning({message: 'Debe seleccionar un &iacute;tem'});
            return;
        }
        if(!data.id){
            $('#messages-block').alertWarning({message: 'El &iacute;tem seleccionado no esta creado'});
            return;
        }
        bootbox.confirm("\u00BFEst\u00E1 seguro que desea eliminar el &iacute;tem seleccionado?", $.proxy(this._deleteCallback, this, data));
    },

    _deleteCallback: function (data, result) {
        if (result) {
            $.ajax({
                method: 'DELETE',
                url: 'api/oficinas/' + data.id,
                dataType: 'json',
                statusCode: {
                    400: $.proxy(this._deleteValidationCallback, this)
                },
                success: $.proxy(this._deleteSuccessCallback, this, data),
                error: $.proxy(this._deleteErrorCallback, this)
            });
        }
    },

    _deleteValidationCallback: function (jqXHR) {
        $('#messages-block').alertWarning({message: jqXHR.responseText});
        this.element.find('button[type=submit].btn').prop('disabled', false).removeClass('disabled');
    },

    _deleteSuccessCallback: function (obj, textStatus, jqXHR) {
        $('#messages-block').alertSuccess({message: 'Se ha dado de baja la oficina seleccionada'});
        this._getData();
    },

    _deleteErrorCallback: function (jqXHR, textStatus, errorThrown) {
        $('#messages-block').alertDanger({message: jqXHR.responseText, keepVisible: true});
        this.element.find('button[type=submit].btn').prop('disabled', false).removeClass('disabled');
    },

    _search: function (e) {
        e.preventDefault();
        this.formFilter.formValidation('disableSubmitButtons', false);
        const obj = this.formFilter.serializeObject();
        $.teco.urlFilter = $.param(obj);
        this._getData();
    },

    _clear: function (e, obj) {
        e.preventDefault();
        this.formFilter.find('#tipo-filter').val(null).trigger('change');
        this.formFilter.find('#provincia-filter').val(null).trigger('change');
        this.formFilter.find('#nombre-filter').val(null).trigger('change');
        this.formFilter.formValidation().data('formValidation').resetForm(true);
    },

    _read: function (e, data) {
        e.preventDefault();
        if (!data) {
            $('#messages-block').alertWarning({message: 'Debe seleccionar un &iacute;tem'});
            return;
        }
        if(!data.id){
            $('#messages-block').alertWarning({message: 'El &iacute;tem seleccionado no esta creado'});
            return;
        }
        $.openModal('oficina/' + data.id + '/show');
    },

    _getItemValidationCallback: function (jqXHR) {
        $('#messages-block').alertWarning({message: jqXHR.responseText[0]});
    },

    _getItemErrorCallback: function (jqXHR, textStatus, errorThrown) {
        $('#messages-block').alertDanger({message: jqXHR.responseText[0], keepVisible: true});
    },

    destroy: function () {
    }

});
