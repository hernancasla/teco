//creamos la base de datos tienda y el objeto SHOP donde iremos almacenando la info
var sqlite3 = require('sqlite3').verbose(),
    db = new sqlite3.Database('tienda'),
    OFICINA = {},
    USER = {};
var bcrypt = require('bcrypt');

//elimina y crea las tablas
OFICINA.createTables = function()
{
    db.run("DROP TABLE IF EXISTS OFICINA");
    db.run("CREATE TABLE IF NOT EXISTS OFICINA (ID INTEGER PRIMARY KEY AUTOINCREMENT, TIPO TEXT, NOMBRE TEXT," +
        "PROVINCIA TEXT, LOCALIDAD TEXT, DIRECCION TEXT, HORARIO TEXT, COORDENADAS TEXT, COMPRAR_LINEAS TEXT," +
        "COMPARAR_CELULAR TEX,PASAR_PERSONAL TEX,MEJORAR_PLAN TEXT,RECUPERAR_CHIP TEXT)");
    db.run("CREATE TABLE IF NOT EXISTS USER (ID INTEGER PRIMARY KEY AUTOINCREMENT, TIPO TEXT, NAME TEXT," +
        "PASSWORD TEXT)");
    console.log("Las tablas han sido correctamente creadas");
}

OFICINA.delete = function(id,callback)
{
    try{
        var stmt = db.prepare("DELETE FROM OFICINA WHERE ID = ?");
        stmt.run(id);
        stmt.finalize();
        callback(null,id);
    } catch(err){
        callback(err,null);
    }

}
OFICINA.update = function(data,callback)
{
    try {
        var stmt = db.prepare("UPDATE OFICINA " +
            "SET TIPO=?, NOMBRE=?, PROVINCIA=?, LOCALIDAD=?, DIRECCION=?, HORARIO=?, COORDENADAS=?, COMPRAR_LINEAS=?, COMPARAR_CELULAR=?, PASAR_PERSONAL=?, MEJORAR_PLAN=?, RECUPERAR_CHIP=? WHERE ID=?");
        stmt.run(data.tipo, data.nombre, data.provincia, data.localidad, data.direccion,
            data.horario, data.coordenadas, data.comprarLineas,
            data.comprarCelular, data.pasarPersonal, data.mejorarPlan, data.recuperarChip, data.id);
        stmt.finalize();
        callback(null,data);

    } catch(err){
        callback(err,null);
    }
}
OFICINA.create = function(officeData,callback)
{
    try {
        var stmt = db.prepare("INSERT INTO OFICINA VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
        stmt.run(null, officeData.tipo, officeData.nombre, officeData.provincia, officeData.localidad, officeData.direccion,
            officeData.horario, officeData.coordenadas, officeData.comprarLineas,
            officeData.comprarCelular, officeData.pasarPersonal, officeData.mejorarPlan, officeData.recuperarChip);
        stmt.finalize();
        callback(null,officeData);

    } catch(err){
        callback(err,null);
    }
}
OFICINA.getTiposOficina = function(callback)
{
    db.all("SELECT TIPO FROM OFICINA GROUP BY TIPO", function(err, rows) {
        if(err)
        {
            callback(err,null);
        }
        else
        {
            callback(null, rows.map(function(data){
                return {tipo:data.TIPO};
            }));
        }
    });
}
OFICINA.getProvincias = function(callback)
{
    db.all("SELECT PROVINCIA FROM OFICINA GROUP BY PROVINCIA", function(err, rows) {
        if(err)
        {
            callback(err,null);
        }
        else
        {
            callback(null, rows.map(function(data){
                return {provincia:data.PROVINCIA};
            }));
        }
    });
}
OFICINA.getLocalidades = function(callback)
{
    db.all("SELECT LOCALIDAD FROM OFICINA GROUP BY LOCALIDAD", function(err, rows) {
        if(err)
        {
            callback(err,null);
        }
        else
        {
            callback(null, rows.map(function(data){
                return {localidad:data.LOCALIDAD};
            }));
        }
    });
}
OFICINA.getOficinasByFilter = function(filter,callback)
{
    var sql = "SELECT * FROM OFICINA WHERE 1=1 ";
    var params = [];
    if(filter.tipo){
        sql = sql.concat("AND TIPO LIKE ? ");
        params.push("%"+filter.tipo+"%");
    }
    if(filter.nombre){
        sql = sql.concat("AND NOMBRE LIKE ? ");
        params.push("%"+filter.nombre+"%");
    }
    if(filter.provincia){
        sql = sql.concat("AND PROVINCIA LIKE ? ");
        params.push("%"+filter.provincia+"%");
    }
    if(filter.comprarLineas){
        sql = sql.concat("AND COMPRAR_LINEAS = ? ");
        params.push(filter.comprarLineas);
    }
    if(filter.comprarCelular){
        sql = sql.concat("AND COMPARAR_CELULAR = ? ");
        params.push(filter.comprarCelular);
    }
    if(filter.pasarPersonal){
        sql = sql.concat("AND PASAR_PERSONAL = ? ");
        params.push(filter.pasarPersonal);
    }
    if(filter.mejorarPlan){
        sql = sql.concat("AND MEJORAR_PLAN = ? ");
        params.push(filter.mejorarPlan);
    }
    if(filter.recuperarChip){
        sql = sql.concat("AND RECUPERAR_CHIP = ? ");
        params.push(filter.recuperarChip);
    }

    db.all(sql,params,function(err, rows)
    {
        if(err)
        {
            callback(err,null);
        }
        else
        {

            callback(null, rows.map(function(data){
                return {
                    tipo:data.TIPO,
                    nombre:data.NOMBRE,
                    provincia:data.PROVINCIA,
                    localidad:data.LOCALIDAD,
                    direccion:data.DIRECCION,
                    horario:data.HORARIO,
                    coordenadas:data.COORDENADAS,
                    comprarLineas:data.COMPRAR_LINEAS,
                    comprarCelular:data.COMPARAR_CELULAR,
                    pasarPersonal:data.PASAR_PERSONAL,
                    mejorarPlan:data.MEJORAR_PLAN,
                    recuperarChip:data.RECUPERAR_CHIP,
                    id:data.ID
                };
            }));
        }
    });
}

USER.create = function(data,callback)
{
    try {
        bcrypt.hash(data.password, 10, function (err, hash) {
            if (err) {
                throw next(err);
            }
            var stmt = db.prepare("INSERT INTO USER (ID,USER_NAME,PASSWORD) VALUES (?,?,?)");
            stmt.run(null, data.username, hash);
            stmt.finalize();
            callback(null,data);
        });
    } catch(err){
        callback(err,null)
    }
}
OFICINA.getOficinasById = function(oficinaId,callback)
{
    stmt = db.prepare("SELECT * FROM OFICINA WHERE ID = ?");
    stmt.bind(oficinaId);
    stmt.get(function(err, row)
    {
        if(err)
        {
            callback(err,null);
        }
        else
        {
            if(row)
            {
                callback("",{
                    tipo:row.TIPO,
                    nombre:row.NOMBRE,
                    provincia:row.PROVINCIA,
                    localidad:row.LOCALIDAD,
                    direccion:row.DIRECCION,
                    horario:row.HORARIO,
                    coordenadas:row.COORDENADAS,
                    comprarLineas:row.COMPRAR_LINEAS,
                    comprarCelular:row.COMPARAR_CELULAR,
                    pasarPersonal:row.PASAR_PERSONAL,
                    mejorarPlan:row.MEJORAR_PLAN,
                    recuperarChip:row.RECUPERAR_CHIP,
                    id:row.ID
                });
            }
            else
            {
                console.log("La oficina no existe");
                callback("",null);
            }
        }
    });
}
USER.getByUserAndPass = function(username,callback)
{
    stmt = db.prepare("SELECT ID,USER_NAME,PASSWORD FROM USER WHERE USER_NAME = ?");
    stmt.bind(username);
    stmt.get(function(err, row)
    {
        if(err)
        {
            callback(err,null);
        }
        else
        {
            if(row)
            {
                callback("",{
                    name:row.USER_NAME,
                    password:row.PASSWORD,
                    id:row.ID
                });
            }
            else
            {
                console.log("El usuario no existe");
                callback("",null);
            }
        }
    });
}
//authenticate input against database
USER.authenticate = function (username, password, callback) {
    USER.getByUserAndPass(username, function(err, data)
    {
        if (err) {
            return callback(err,null)
        } else if (!data) {
            var err = new Error('User not found.');
            err.status = 401;
            return callback(err,null);
        }
        bcrypt.compare(password, data.password, function (err, result) {
            if (result === true) {
                return callback(null, data);
            } else {
                return callback();
            }
        });

    });
}
USER.createTables = function()
{
    db.run("CREATE TABLE IF NOT EXISTS USER (ID INTEGER PRIMARY KEY AUTOINCREMENT, USER_NAME TEXT," +
        "PASSWORD TEXT)");
    console.log("Las tablas han sido correctamente creadas");
}
//exportamos el modelo para poder utilizarlo con require
module.exports = {OFICINA:OFICINA,USER:USER};
