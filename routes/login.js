var express = require('express');
var router = express.Router();
var sqlite = require('../models/sqlite');
var session = require('express-session');


// GET route for reading data
router.get('/', function (req, res, next) {
    if(req.session.userId)
    {
        res.redirect('/backoffice/oficina');
    }
    res.render('login/login', {loginErrorMsg:"",logoutMsg:"",session:{}});
});

router.get("/user/createModel", function(req, res){
    sqlite.USER.createTables();
    res.end();
});
//POST route for updating data
router.post('/signup', function (req, res, next) {
    if (req.body.username &&
        req.body.password) {

        var userData = {
            username: req.body.username,
            password: req.body.password
        }

        sqlite.USER.create(userData, function (error, user) {
            if (error) {
                return res.render('partials/error', {
                    error : error, message:"ERROR signup"
                });
            } else {
                req.session.userId = user.id;
                return res.redirect('/backoffice/oficina');
            }
        });

    } else {
        var err = new Error('All fields required.');
        err.status = 400;
        return res.render('partials/error', {
            error : error, message:"ERROR signup"
        });
    }
});
router.post('/signin', function (req, res, next) {

    if (req.body.username && req.body.password) {
        sqlite.USER.authenticate(req.body.username, req.body.password, function (error, user) {
            if (error || !user) {
                res.render('login/login', {loginErrorMsg:"Usuario o Password incorrectos",logoutMsg:"",session:{}});
            } else {
                req.session.userId = user.id;
                return res.redirect('/backoffice/oficina');
            }
        });
    }
});


// GET for logout logout
router.get('/logout', function (req, res, next) {
    if(!req.session)
    {
        return res.redirect('/backoffice/');
    }
    if (req.session) {
        // delete session object
        req.session.destroy(function (err) {
            if (err) {
                return res.render('partials/error', {
                    error : error, message:"ERROR logout"
                });
            } else {
                return res.redirect('/backoffice');
            }
        });
    }
});

module.exports = router;