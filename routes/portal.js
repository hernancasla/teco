var express = require('express');
var router = express.Router();
var sqlite = require('../models/sqlite');
var url = require('url');


router.get('/', function(req, res, next) {
    res.render('portal/select-option', {});
});
router.get('/index', function(req, res, next) {
    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    res.render('portal/index', {filter:JSON.stringify(query)});
});
module.exports = router;