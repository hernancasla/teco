var express = require('express');
var sqlite = require('../models/sqlite');
var url = require('url');
var logger = require('tracer').console();



var router = express.Router();

router.get('/oficina', function(req, res, next) {
    if(!req.session.userId)
    {
        res.redirect("/backoffice");
    }
    sqlite.OFICINA.getTiposOficina(function(error, data)
    {
        if(error){
            return res.render('partials/error', {
                error : error, message:"ERROR oficina"
            });
        }
        var tipos = data;
        sqlite.OFICINA.getProvincias(function(error,data){
            res.render('oficina/index', { title: 'Oficinas',tipos: tipos,provincias:data,session:req.session});
        })
    });
});
router.get('/oficina/:id/show', function(req, res, next) {
    if(!req.session.userId)
    {
        res.redirect("/backoffice");
    }
    sqlite.OFICINA.getOficinasById(req.params.id,function(error, data)
    {
        if(error){
            return res.render('partials/error', {
                error : error, message:"ERROR show"
            });
        }
        if(!data)
            return res.render('partials/error', { message: 'Oficina inexistente', error:{}});

        res.render('oficina/show', { title: 'Oficina',item: data});

    });
});
router.get('/oficina/create/', function(req, res, next) {
    if(!req.session.userId)
    {
        res.redirect("/backoffice");
    }
    sqlite.OFICINA.getTiposOficina(function(err, tipos)
    {
        if(err){
            return res.render('partials/error', {
                error : err, message:"ERROR oficina"
            });
        }
        sqlite.OFICINA.getProvincias(function(err,provincias){
            if(err){
                return res.render('partials/error', {
                    error : err, message:"ERROR oficina"
                });
            }

            sqlite.OFICINA.getLocalidades(function(err,localidades){
                if(err){
                    return res.render('partials/error', {
                        error : err, message:"ERROR oficina"
                    });
                }
                res.render('oficina/form', { title: 'Oficina',item: {},tipos: tipos,provincias:provincias,localidades:localidades});
            })
        })
    });
});
router.get('/oficina/:id/edit/', function(req, res, next) {
    if(!req.session.userId)
    {
        res.redirect("/backoffice");
    }
    sqlite.OFICINA.getOficinasById(req.params.id,function(err, oficina)
    {
        if(err){
            return res.render('partials/error', {
                error : error, message:"ERROR edit"
            });
        }
        if(!oficina)
            return res.render('partials/error', { message: 'Oficina inexistente', error:{status:404,stack:""}});

        sqlite.OFICINA.getTiposOficina(function(err, tipos)
        {
            if(err){
                return res.render('partials/error', {
                    error : err, message:"ERROR oficina"
                });
            }
            sqlite.OFICINA.getProvincias(function(err,provincias){
                if(err){
                    return res.render('partials/error', {
                        error : err, message:"ERROR oficina"
                    });
                }

                sqlite.OFICINA.getLocalidades(function(err,localidades){
                    if(err){
                        return res.render('partials/error', {
                            error : err, message:"ERROR oficina"
                        });
                    }
                    res.render('oficina/form', { title: 'Oficina',item: oficina,tipos: tipos,provincias:provincias,localidades:localidades});
                })
            })
        });
    });
});
router.get("/createModel", function(req, res){
    if(!req.session.userId)
    {
        res.redirect("/backoffice");
    }
    sqlite.OFICINA.createTables();
    res.end();
});
router.post("/api/oficinas", function(req, res)
    {
        if(!req.session.userId)
        {
            return res.send({});
        }
        sqlite.OFICINA.create(req.body,function(err, data)
        {
            if(err) {
                return res.error(err);
            }
            res.send(req.body);

        });
    });
router.get("/api/oficinas", function(req, res){
    try {
        var url_parts = url.parse(req.url, true);
        var query = url_parts.query;
        sqlite.OFICINA.getOficinasByFilter(query, function (err, data) {
            if(err) {
                return res.error(err);
            }

            res.send(data);

        });
    } catch(err){
        logger.error(err);
        res.error(err);
    }
});


router.get("/api/oficinas/:id", function(req, res){
    if(!req.session.userId)
    {
        return res.send({});
    }
    sqlite.OFICINA.getOficinasById(req.params.id,function(err, data)
        {
            if(err) {
                return res.error(err);
            }
            res.send(data);
        });
    });

router.put("/api/oficinas/:id", function(req, res){
    if(!req.session.userId)
    {
        return res.send({});
    }
    sqlite.OFICINA.update(req.body,function(err, data)
    {
        if(err) {
            return res.error(err);
        }
        res.send(data);
    });
});

router.delete("/api/oficinas/:id", function(req, res){
    if(!req.session.userId)
    {
        return res.send({});
    }
    sqlite.OFICINA.delete(req.params.id,function(err, data)
    {
        if(err) {
            return res.error(err);
        }
        res.send({id:req.params.id});
    });
});

module.exports = router;