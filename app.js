var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var SQLiteStore = require('connect-sqlite3')(session);

var oficina = require('./routes/oficina');
var login = require('./routes/login');
var portal = require('./routes/portal');
var index = require('./routes/index');




var app = express();

var sqlite3 = require('sqlite3').verbose(),
    db = new sqlite3.Database('tienda');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
    secret: 'work hard',
    cookie: {maxAge: 600000},
    resave: true,
    saveUninitialized: false,
    store: new SQLiteStore
}));
app.use('/backoffice', oficina);
app.use('/backoffice',login);
app.use('/portal',portal);
app.use('/',index);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('partials/error');
});
// require('./routes')(app);
module.exports = app;
